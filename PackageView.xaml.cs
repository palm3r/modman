﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Modman
{
  public partial class PackageView : UserControl
  {
    public PackageView()
    {
      InitializeComponent();
    }
  }

  public class FileSizeConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      if (value is long)
      {
        var size = (long)value;
        var result = string.Empty;

        if (size < Math.Pow(1024, 1))
          result = string.Format("{0}B", size);
        else if (size < Math.Pow(1024, 2))
          result = string.Format("{0:f2}KB", size / Math.Pow(1024, 1));
        else if (size < Math.Pow(1024, 3))
          result = string.Format("{0:f2}MB", size / Math.Pow(1024, 2));
        else if (size < Math.Pow(1024, 4))
          result = string.Format("{0:f2}GB", size / Math.Pow(1024, 3));
        else if (size < Math.Pow(1024, 5))
          result = string.Format("{0:f2}TB", size / Math.Pow(1024, 4));

        return result;
      }
      return DependencyProperty.UnsetValue;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
