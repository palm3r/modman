﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Runtime.InteropServices;

namespace Modman
{
  public partial class App : Application
  {
    protected override void OnStartup(StartupEventArgs e)
    {
      var c = Process.GetCurrentProcess();
      var o = Process.GetProcessesByName(c.ProcessName)
        .SingleOrDefault(p => p.Id != c.Id
          && p.StartInfo.WorkingDirectory == c.StartInfo.WorkingDirectory);

      if (o != null)
      {
        ShowWindow(o.MainWindowHandle, 1);
        SetForegroundWindow(o.MainWindowHandle);
        Shutdown();
      }
      else
        base.OnStartup(e);
    }

    [DllImport("User32.dll")]
    private static extern int ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("User32.dll")]
    private static extern bool SetForegroundWindow(IntPtr hWnd);
  }
}
