﻿using System;
using System.Runtime.InteropServices;

namespace Modman.Utils
{
  internal static class Win32
  {
    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr LoadLibrary(string fileName);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool FreeLibrary(IntPtr hModule);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern bool CreateSymbolicLinkW(string lpSymlinkFileName,
      string lpTargetFileName, int dwFlags);
  }
}
