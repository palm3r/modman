﻿using System.Linq;
using System.IO;

namespace Modman.Utils
{
  internal static class Hash
  {
    internal static string FromFile(string path)
    {
      if (!File.Exists(path))
        return null;

      using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
      {
        using (var md5 = System.Security.Cryptography.MD5.Create())
        {
          return md5.ComputeHash(fs)
            .Select(b => b.ToString("x2"))
            .Aggregate("", (str, b) => str + b);
        }
      }
    }
  }
}
