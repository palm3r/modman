﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace Modman.Utils
{
  internal static class FileManager
  {
    public static bool UseSymlink { get; private set; }

    static FileManager()
    {
      var kernel32 = Win32.LoadLibrary("kernel32.dll");
      if (kernel32 != IntPtr.Zero)
      {
        UseSymlink = Win32.GetProcAddress(kernel32, "CreateSymbolicLinkW") != IntPtr.Zero;
        Win32.FreeLibrary(kernel32);
      }
    }

    public static bool Install(string sourceFile, string targetFile, bool overwrite)
    {
      if (UseSymlink)
      {
        if (File.Exists(targetFile))
        {
          if (!overwrite)
            return false;

          File.Delete(targetFile);
        }
        return Win32.CreateSymbolicLinkW(targetFile, sourceFile, 0);
      }
      else
      {
        File.Copy(sourceFile, targetFile, overwrite);
      }
      return true;
    }

    public static bool Delete(string path)
    {
      if (Directory.Exists(path))
      {
        Directory.Delete(path);
        return !Directory.Exists(path);
      }
      else if (File.Exists(path))
      {
        File.Delete(path);
        return !File.Exists(path);
      }
      return true;
    }
  }
}
