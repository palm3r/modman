﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Modman.Models;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Modman.ViewModels
{
  class MainWindowViewModel : NotificationObject
  {
    public ObservableCollection<Profile> Profiles
    {
      get { return GetValue<ObservableCollection<Profile>>("Profiles", new ObservableCollection<Profile>()); }
      set { SetValue("Profiles", value); }
    }

    public Profile SelectedProfile
    {
      get { return GetValue<Profile>("SelectedProfile"); }
      set
      {
        SetValue("SelectedProfile", value)
          .WhenChanged(x =>
            {
              EditProfileCommand.RaiseCanExecuteChanged();
              DeleteProfileCommand.RaiseCanExecuteChanged();
            });
      }
    }

    public Command AddProfileCommand { get; private set; }
    public Command EditProfileCommand { get; private set; }
    public Command DeleteProfileCommand { get; private set; }

    public string Filter
    {
      get { return GetValue<string>("Filter"); }
      set
      {
        SetValue("Filter", value)
          .WhenChanged(x =>
          {
            foreach (var package in SelectedProfile.Packages)
            {
              package.Visibility = package.Name.ToLower().Contains(value.ToLower())
                ? Visibility.Visible
                : Visibility.Collapsed;
            }
          });
      }
    }

    public MainWindowViewModel()
      : base()
    {
      AddProfileCommand = new Command(
        x =>
        {
          AddProfileCommand.IsEnabled = false;
          var profile = new Profile();
          var window = new ProfileWindow(App.Current.MainWindow.Title +
            " - Create Profile", profile)
            {
              Owner = App.Current.MainWindow,
              WindowStartupLocation = WindowStartupLocation.CenterOwner,
            };
          if (window.ShowDialog() ?? false)
          {
            using (var repos = Repository.Begin())
            {
              Directory.EnumerateDirectories(profile.Source)
                .Select(path => new Package(profile, path))
                .ToList()
                .ForEach(package => profile.Packages.Add(package));

              repos.DbContext.Profiles.Add(profile);
              repos.DbContext.SaveChanges();
            }

            Profiles.Add(profile);
            SelectedProfile = profile;

            Task.Factory.StartNew(() => profile.Packages.ToList()
              .ForEach(package => package.Initialize()));
          }
          AddProfileCommand.IsEnabled = true;
        },
        x => true);

      EditProfileCommand = new Command(
        x =>
        {
          EditProfileCommand.IsEnabled = false;
          var window = new ProfileWindow(App.Current.MainWindow.Title +
            " - Edit Profile", SelectedProfile)
            {
              Owner = App.Current.MainWindow,
              WindowStartupLocation = WindowStartupLocation.CenterOwner,
            };
          if (window.ShowDialog() ?? false)
          {
            using (var repos = Repository.Begin())
            {
              repos.DbContext.Profiles.Attach(SelectedProfile);
              repos.DbContext.Entry(SelectedProfile).State = System.Data.EntityState.Modified;
              repos.DbContext.SaveChanges();
            }
          }
          EditProfileCommand.IsEnabled = true;
        },
        x =>
        {
          if (SelectedProfile == null)
            return false;

          if (SelectedProfile.Packages
            .Count(profile => profile.Status == PackageStatus.INSTALLING
              || profile.Status == PackageStatus.UNINSTALLING) > 0)
            return false;

          return true;
        }
        );

      DeleteProfileCommand = new Command(
        x =>
        {
          var installedPackages = SelectedProfile.Packages
            .Where(p => p.Status == PackageStatus.INSTALLED)
            .ToList();
          var result = MessageBox.Show(
            "Do you really want to delete this profile?\r\n" +
            "Installed packages will be uninstalled automatically.",
            App.Current.MainWindow.Title, MessageBoxButton.YesNo);
          if (result != MessageBoxResult.Yes)
            return;

          DeleteProfileCommand.IsEnabled = false;
          var bw = new BackgroundWorker();
          bw.DoWork += (s, e) =>
            {
              installedPackages
                .ForEach(package =>
                  {
                    var task = package.Uninstall();
                    task.Wait();
                  });
            };
          bw.RunWorkerCompleted += (s, e) =>
            {
              using (var repos = Repository.Begin())
              {
                repos.DbContext.Profiles.Attach(SelectedProfile);
                repos.DbContext.Profiles.Remove(SelectedProfile);
                repos.DbContext.SaveChanges();
              }
              Profiles.Remove(SelectedProfile);
              DeleteProfileCommand.IsEnabled = true;
            };
          bw.RunWorkerAsync();
        },
        x =>
        {
          if (SelectedProfile == null)
            return false;

          if (SelectedProfile.Packages
            .Count(profile => profile.Status == PackageStatus.INSTALLING
              || profile.Status == PackageStatus.UNINSTALLING) > 0)
            return false;

          return true;
        }
        );

      using (var repos = Repository.Begin())
      {
        foreach (var profile in repos.DbContext.Profiles
          .Include(profile => profile.Packages.Select(package => package.Journals)))
        {
          var removedPackages = profile.Packages
            .Where(package => package.Journals.Count == 0)
            .ToList()
            .Select(package => repos.DbContext.Packages.Remove(package));
            
          if (removedPackages.Count() > 0)
            repos.DbContext.SaveChanges();

          var dir = new DirectoryInfo(profile.Source);
          var addedPackages = dir.EnumerateDirectories()
            .Where(d => d.EnumerateFiles("*", SearchOption.AllDirectories)
              .Aggregate(0L, (size, file) => size += file.Length) > 0)
            .Where(d => profile.Packages.Count(p => p.Name == d.Name) == 0)
            .Select(d => repos.DbContext.Packages.Add(new Package(profile, d.FullName)))
            .ToList();
          if (addedPackages.Count > 0)
            repos.DbContext.SaveChanges();

          Profiles.Add(profile);
        }
      }

      SelectedProfile = Profiles.Count > 0 ? Profiles.FirstOrDefault() : null;

      Task.Factory.StartNew(() =>
        {
          Profiles.SelectMany(profile => profile.Packages)
            .ToList()
            .ForEach(package => package.Initialize());
        });
    }
  }
}
