﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modman.Models;
using System.Collections.ObjectModel;
using System.IO;

namespace Modman.ViewModels
{
  public class ProfileViewModel : NotificationObject
  {
    public Profile Model
    {
      get { return GetValue(() => Model); }
      set { SetValue(() => Model, value); }
    }

    public int Id
    {
      get { return Model.Id; }
      set { Model.Id = value; }
    }

    public string Name
    {
      get { return Model.Name; }
      set { Model.Name = value; }
    }

    public string Target
    {
      get { return Model.Target; }
      set { Model.Target = value; }
    }

    public string Source
    {
      get { return Model.Source; }
      set
      {
        if (Model.Source != null)
        {
          Model.Source = value;
          UpdatePackages();
          RaisePropertyChanged(() => Source, () => Packages);
        }
      }
    }

    public ObservableCollection<PackageViewModel> Packages
    {
      get { return GetValue(() => Packages); }
      set { SetValue(() => Packages, value); }
    }

    public ProfileViewModel(Profile model)
      : base()
    {
      Model = model;
      /*
      Id = model.Id;
      Name = model.Name;
      Target = model.Target;
      Source = model.Source;
      */
      UpdatePackages();
    }

    public ProfileViewModel()
      : this(new Profile())
    {
    }

    private void UpdatePackages()
    {
      var packages = new ObservableCollection<PackageViewModel>();

      if (Id > 0)
      {
        using (var repos = Repository.Begin())
        {
          var profile = repos.Current.Profiles
            .Include(prf => prf.Packages.Select(pkg => pkg.Journals))
            .SingleOrDefault(prf => prf.Id == Id);

          profile.Packages
            .Select(package => new PackageViewModel(this, package))
            .ToList()
            .ForEach(vm => packages.Add(vm));
        }
      }

      if (!string.IsNullOrEmpty(Source))
      {
        Directory.EnumerateDirectories(Source, "*", SearchOption.TopDirectoryOnly)
          .ToList()
          .ForEach(file =>
          {
            var package = new Package(Model, file);
            if (packages.Where(vm => vm.Name == package.Name).Count() == 0)
              packages.Add(new PackageViewModel(this, package));
          });
      }

      Packages = packages;
    }
  }
}
