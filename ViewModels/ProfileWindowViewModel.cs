﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modman.Models;
using System.Windows.Input;
using System.Windows;
using System.Windows.Forms;
using System.IO;

namespace Modman.ViewModels
{
  public class ProfileWindowViewModel : NotificationObject
  {
    public string Name
    {
      get { return GetValue<string>("Name"); }
      set
      {
        SetValue("Name", value)
          .WhenChanged(x => SubmitCommand.RaiseCanExecuteChanged());
      }
    }

    public string Target
    {
      get { return GetValue<string>("Target"); }
      set
      {
        SetValue("Target", value)
          .WhenChanged(x => SubmitCommand.RaiseCanExecuteChanged());
      }
    }

    public string Source
    {
      get { return GetValue<string>("Source"); }
      set
      {
        SetValue("Source", value)
          .WhenChanged(x => SubmitCommand.RaiseCanExecuteChanged());
      }
    }

    public Command BrowseTargetCommand { get; private set; }
    public Command BrowseSourceCommand { get; private set; }
    public Command SubmitCommand { get; private set; }

    Window _window;
    Profile _profile;

    public ProfileWindowViewModel(Window window, Profile profile)
      : base()
    {
      _window = window;
      _profile = profile;

      BrowseTargetCommand = new Command(
        x =>
        {
          var dlg = new FolderBrowserDialog()
            {
              RootFolder = Environment.SpecialFolder.Desktop,
              SelectedPath = Target,
              ShowNewFolderButton = true,
            };
          if (dlg.ShowDialog() == DialogResult.OK)
            Target = dlg.SelectedPath;
        },
        x => true
        );

      BrowseSourceCommand = new Command(
        x =>
        {
          var dlg = new FolderBrowserDialog()
            {
              RootFolder = Environment.SpecialFolder.Desktop,
              SelectedPath = Source,
              ShowNewFolderButton = true,
            };
          if (dlg.ShowDialog() == DialogResult.OK)
            Source = dlg.SelectedPath;
        },
        x => true
        );

      SubmitCommand = new Command(
        x =>
        {
          _profile.Name = Name;
          _profile.Target = Path.GetFullPath(Target);
          _profile.Source = Path.GetFullPath(Source);
          _window.DialogResult = true;
          _window.Close();
        },
        x =>
        {
          try
          {
            var mainWindow = App.Current.MainWindow as MainWindow;
            var viewModel = mainWindow.DataContext as MainWindowViewModel;
            var profiles = viewModel.Profiles;

            var target = Path.GetFullPath(Target);
            var source = Path.GetFullPath(Source);
            var conflictedPackage = profiles.SingleOrDefault(p => p.Id != _profile.Id && p.Name == Name);
            return Name.Length > 0 && conflictedPackage == null
              && Target.Length > 0 && Directory.Exists(Path.GetFullPath(Target))
              && Source.Length > 0 && Directory.Exists(Path.GetFullPath(Source));
          }
          catch
          {
            return false;
          }
        });

      Name = _profile.Name;
      Target = _profile.Target;
      Source = _profile.Source;
    }
  }
}
