﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Modman.Models;
using System.Windows;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using Modman.Utils;
using System.Collections.ObjectModel;

namespace Modman.ViewModels
{
  public enum PackageStatus
  {
    Initializing,
    Incomplete,
    Uninstalled,
    Uninstalling,
    Installed,
    Installing,
  }

  public class PackageViewModel : NotificationObject
  {
    public Package Model
    {
      get { return GetValue(() => Model); }
      set { SetValue(() => Model, value); }
    }

    public int Id
    {
      get { return Model.Id; }
      set { Model.Id = value; }
    }

    public string Name
    {
      get { return Model.Name; }
      set { Model.Name = value; }
    }

    public ProfileViewModel Parent
    {
      get { return GetValue(() => Parent); }
      set { SetValue(() => Parent, value); }
    }
    /*
    public ObservableCollection<Journal> Journals
    {
      get { return GetValue(() => Journals); }
      set { SetValue(() => Journals, value); }
    }
    */
    public Visibility Visibility
    {
      get { return GetValue(() => Visibility, Visibility.Visible); }
      set { SetValue(() => Visibility, value); }
    }

    public PackageStatus Status
    {
      get { return GetValue(() => Status); }
      set { SetValue(() => Status, value); }
    }

    public double ProgressMaximum
    {
      get { return GetValue(() => ProgressMaximum); }
      set { SetValue(() => ProgressMaximum, value); }
    }

    public double ProgressValue
    {
      get { return GetValue(() => ProgressValue); }
      set { SetValue(() => ProgressValue, value); }
    }

    public TimeSpan ElapsedTime
    {
      get { return GetValue(() => ElapsedTime); }
      set { SetValue(() => ElapsedTime, value); }
    }

    public TimeSpan RemainingTime
    {
      get { return GetValue(() => RemainingTime); }
      set { SetValue(() => RemainingTime, value); }
    }

    public string Path
    {
      get { return System.IO.Path.Combine(Parent.Source, Name); }
    }

    public IEnumerable<string> Files
    {
      get
      {
        return Directory.EnumerateFiles(Path, "*", SearchOption.AllDirectories)
          .Select(file => file.Substring(Path.Length + 1));
      }
    }

    public Command InstallCommand { get; private set; }
    public Command UninstallCommand { get; private set; }

    Task _task;
    CancellationTokenSource _cancel;

    public PackageViewModel(ProfileViewModel parent, Package model)
      : base()
    {
      Model = model;
      /*
      Id = model.Id;
      Name = model.Name;
      */
      Parent = parent;

      InstallCommand = new Command(
        x => Install(),
        x => Status == PackageStatus.Uninstalled
        );

      UninstallCommand = new Command(
        x => Uninstall(),
        x => Status == PackageStatus.Installed
        );

      var files = Files.ToList();
      ProgressValue = 0;
      ProgressMaximum = files.Count;

      using (var repos = Repository.Begin())
      {
        var pkg = repos.Current.Packages
          .Include(p => p.Journals)
          .SingleOrDefault(p => p.Id == Id);

        if (pkg != null)
        {
          ProgressValue = pkg.Journals.Count;
          Status = pkg.Journals.Count == files.Count
            ? PackageStatus.Installed : PackageStatus.Incomplete;
        }
        else
        {
          Status = PackageStatus.Uninstalled;
        }
      }
    }

    private void Install()
    {
      Status = PackageStatus.Installing;
      _cancel = new CancellationTokenSource();
      var start = DateTime.Now;

      _task = new Task(() =>
      {
        int i = 0;
        var files = Files.ToList();
        foreach (var file in files)
        {
          InstallFile(file);
          ElapsedTime = DateTime.Now - start;
          RemainingTime = TimeSpan.FromMilliseconds((ElapsedTime.TotalMilliseconds / ++i) * (files.Count - i));
        }
      }, _cancel.Token);

      // failed
      _task.ContinueWith(task =>
      {
        using (var repos = Repository.Begin())
        {
          var files = repos.Current.Journals
            .Include(j => j.Package)
            .Where(j => j.Package.Id == Id)
            .Select(j => j.Path);
          foreach (var file in files)
            UninstallFile(file);
        }
      }, TaskContinuationOptions.OnlyOnFaulted);

      // completed
      _task.ContinueWith(task =>
      {
        using (var repos = Repository.Begin())
        {
          var journals = repos.Current.Journals
            .Include(j => j.Package)
            .Where(j => j.Package.Id == Id);
          ProgressValue = ProgressMaximum = journals.Count();
        }
        Status = PackageStatus.Installed;
      }, TaskContinuationOptions.OnlyOnRanToCompletion);

      // finally
      _task.ContinueWith(task =>
      {
      });

      _task.Start();
    }

    private void Uninstall()
    {
      Status = PackageStatus.Uninstalling;
      _cancel = new CancellationTokenSource();
      var start = DateTime.Now;

      _task = new Task(() =>
      {
        using (var repos = Repository.Begin())
        {
          var files = repos.Current.Journals
            .Include(j => j.Package)
            .Where(j => j.Package.Id == Id)
            .Select(j => j.Path)
            .ToList();

          int i = 0;
          foreach (var file in files)
          {
            UninstallFile(file);
            ElapsedTime = DateTime.Now - start;
            RemainingTime = TimeSpan.FromMilliseconds((ElapsedTime.TotalMilliseconds / ++i) * (files.Count - i));
          }
        }
      }, _cancel.Token);

      // failed
      _task.ContinueWith(task =>
      {
        using (var repos = Repository.Begin())
        {
          var journals = repos.Current.Journals
            .Include(j => j.Package)
            .Where(j => j.Package.Id == Id && j.IsDeleted)
            .ToList();
          foreach (var journal in journals)
            InstallFile(journal.Path);
        }
      }, TaskContinuationOptions.OnlyOnFaulted);

      // completed
      _task.ContinueWith(task =>
      {
        Status = PackageStatus.Uninstalled;
      }, TaskContinuationOptions.OnlyOnRanToCompletion);

      // finally
      _task.ContinueWith(task =>
      {
        using (var repos = Repository.Begin())
        {
          var package = repos.Current.Packages.Find(Id);
          repos.Current.Packages.Remove(package);
          repos.Current.SaveChanges();
        }
        Id = 0;
        ProgressValue = 0;
      });

      _task.Start();
    }

    private void InstallFile(string path)
    {
      var source = System.IO.Path.Combine(Parent.Source, Name, path);
      var target = System.IO.Path.Combine(Parent.Target, path);
      var dir = System.IO.Path.GetDirectoryName(target);

      if (!Directory.Exists(dir))
        Directory.CreateDirectory(dir);

      using (var repos = Repository.Begin())
      {
        /*
        var package = repos.Current.Packages
          .Include(p => p.Profile)
          .SingleOrDefault(p => p.Id == Id);
        */
        var package = repos.Current.Packages
          .SingleOrDefault(p => p.Id == Id);
        if (package == null)
        {
          package = new Package(Parent.Model, Path);
          repos.Current.Packages.Add(package);
          repos.Current.SaveChanges();
          Id = package.Id;
        }

        var journal = repos.Current.Journals
          .Include(j => j.Package)
          .SingleOrDefault(j => j.Path == path && !j.IsDeleted);
        if (journal == null)
        {
          journal = new Journal(package, path);
          repos.Current.Journals.Add(journal);
          repos.Current.SaveChanges();
        }

        var journalOfOtherPackage = repos.Current.Journals
          .Include(j => j.Package)
          .OrderBy(j => j.DateTime)
          .ToList()
          .LastOrDefault(j => j.Package.Id == package.Id && j.Path == path && !j.IsDeleted);

        if (journalOfOtherPackage == null && File.Exists(target))
        {
          var backup = Backup.Save(target, Parent.Target);
          repos.Current.Backups.Add(backup);
          repos.Current.SaveChanges();
        }

        if (Hash.FromFile(source) != Hash.FromFile(target))
          FileManager.Install(source, target, true);

        ProgressValue++;
      }
    }

    private void UninstallFile(string path)
    {
      var source = System.IO.Path.Combine(Parent.Source, Name, path);
      var target = System.IO.Path.Combine(Parent.Target, path);
      var dir = System.IO.Path.GetDirectoryName(target);

      /*
      var srcHash = Hash.FromFile(source);
      var dstHash = Hash.FromFile(target);
      if (srcHash != null && dstHash != null && srcHash != dstHash)
      {
        if (_msgBoxResult == MessageBoxResult.
        if (MessageBox.Show("This file is modified after installation. Proceed to delete?\r\n" + target,
          "Generic Mod Installer", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
        {
          // TODO: Cancel
        }
      }
      */

      FileManager.Delete(target);

      using (var repos = Repository.Begin())
      {
        var package = repos.Current.Packages
          .Include(p => p.Profile)
          .Single(p => p.Id == Id);

        var journal = package.Journals
          .SingleOrDefault(j => j.Path == path && !j.IsDeleted);
        if (journal != null)
        {
          journal.IsDeleted = true;
          repos.Current.SaveChanges();
        }

        journal = repos.Current.Journals
          .Include(j => j.Package)
          .OrderBy(j => j.DateTime)
          .ToList()
          .LastOrDefault(j => j.Package.Id != Id);
        if (journal != null)
        {
          var recover = System.IO.Path.Combine(Parent.Source, journal.Package.Name, journal.Path);
          FileManager.Install(recover, target, true);
        }
        else
        {
          var backup = repos.Current.Backups.SingleOrDefault(b => b.Path == path);
          if (backup != null)
          {
            backup.Restore(target);
            repos.Current.Backups.Remove(backup);
            repos.Current.SaveChanges();
          }
        }
      }

      var di = new DirectoryInfo(dir);
      while (di.FullName != Parent.Target &&
        di.Exists && di.EnumerateFiles("*", SearchOption.AllDirectories).Count() == 0)
      {
        di.Delete(true);
        di = di.Parent;
      }

      ProgressValue--;
    }
  }
}
