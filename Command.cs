﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Modman
{
  public class Command : NotificationObject, ICommand
  {
    public event EventHandler CanExecuteChanged
    {
      add { _handlers.Add(new WeakReference(value)); }
      remove
      {
        _handlers
          .Where(handler => (EventHandler)handler.Target == value || handler.Target == null)
          .ToList()
          .ForEach(handler => _handlers.Remove(handler));
      }
    }

    public bool IsEnabled
    {
      get { return GetValue<bool>("IsEnabled", true); }
      set
      {
        SetValue("IsEnabled", value)
          .WhenChanged(x => RaiseCanExecuteChanged());
      }
    }

    List<WeakReference> _handlers;
    Action<object> _execute;
    Predicate<object> _canExecute;

    public Command(Action<object> execute, Predicate<object> canExecute)
      : base()
    {
      _handlers = new List<WeakReference>();
      _execute = execute;
      _canExecute = canExecute;
    }

    public void RaiseCanExecuteChanged()
    {
      RaisePropertyChanged("CanExecute");
      var handler = _handlers.FirstOrDefault();
      if (handler != null)
      {
        if (handler.Target != null)
          ((EventHandler)handler.Target)(this, EventArgs.Empty);
        else
          _handlers.Remove(handler);
      }
    }

    public bool CanExecute(object parameter)
    {
      return _canExecute(parameter) && IsEnabled;
    }

    public void Execute(object parameter)
    {
      _execute(parameter);
    }
  }
}
