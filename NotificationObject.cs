﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using System.Linq.Expressions;

namespace Modman
{
  public class NotificationObject : INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;

    public NotificationObject()
    {
    }

    Dictionary<string, object> _values = new Dictionary<string, object>();

    public T GetValue<T>(string key, T defaultValue = default(T))
    {
      if (!_values.ContainsKey(key))
        _values[key] = defaultValue;

      return (T)_values[key];
    }

    public Executer<T> SetValue<T>(string key, T value)
    {
      if (_values.ContainsKey(key) && object.Equals(_values[key], value))
        return new Executer<T>(false, value);

      _values[key] = value;
      RaisePropertyChanged(key);

      return new Executer<T>(true, value);
    }
    /*
    public T GetValue<T>(Expression<Func<T>> expression, T defaultValue = default(T))
    {
      return GetValue<T>((expression.Body as MemberExpression).Member.Name, defaultValue);
    }

    public Executer<T> SetValue<T>(Expression<Func<T>> expression, T value)
    {
      return SetValue<T>((expression.Body as MemberExpression).Member.Name, value);
    }
    */
    public void RaisePropertyChanged(params string[] propertyNames)
    {
      if (PropertyChanged != null)
        foreach (var propertyName in propertyNames)
          PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }
    /*
    public void RaisePropertyChanged(params Expression<Func<object>>[] expressions)
    {
      RaisePropertyChanged(expressions
        .Select(ex => (ex.Body as MemberExpression).Member.Name)
        .ToArray());
    }
    */
    public class Executer<T>
    {
      bool _changed;
      T _value;

      public Executer(bool changed, T value)
      {
        _changed = changed;
        _value = value;
      }

      public Executer<T> WhenNotChanged(Action<T> action)
      {
        if (!_changed)
          action(_value);
        return this;
      }

      public Executer<T> WhenChanged(Action<T> action)
      {
        if (_changed)
          action(_value);
        return this;
      }
    }
  }
}
