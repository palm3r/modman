﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace Modman.Models
{
  public class Backup
  {
    [Key]
    public int Id { get; set; }

    [Required]
    public string Path { get; set; }

    [Required]
    public DateTime DateTime { get; set; }

    [Required, Column(TypeName = "image")]
    public byte[] Bytes { get; set; }

    public Backup()
    {
    }

    public static Backup Save(string filePath, string basePath)
    {
      var path = filePath.Substring(basePath.Length + 1);
      var dateTime = System.DateTime.Now;

      var file = new FileInfo(filePath);
      var buffer = new byte[file.Length];
      using (var fs = file.OpenRead())
      {
        fs.Read(buffer, 0, (int)file.Length);
      }
      var bytes = buffer;

      return new Backup()
        {
          Path = path,
          DateTime = dateTime,
          Bytes = bytes,
        };
    }

    public void Restore(string path)
    {
      var fileInfo = new FileInfo(path);
      using (var fs = fileInfo.OpenWrite())
        fs.Write(Bytes, 0, Bytes.Length);
    }
  }
}
