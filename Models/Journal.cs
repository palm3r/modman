﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Modman.Models
{
  public class Journal : Model
  {
    [Key]
    public int Id
    {
      get { return GetValue<int>("Id", 0); }
      set { SetValue("Id", value); }
    }

    [ForeignKey("Package")]
    public int PackageId
    {
      get { return GetValue<int>("PackageId", 0); }
      set { SetValue("PackageId", value); }
    }

    [ForeignKey("PackageId")]
    public virtual Package Package
    {
      get { return GetValue<Package>("Package", null); }
      set { SetValue("Package", value); }
    }

    [Required]
    public string Path
    {
      get { return GetValue<string>("Path", ""); }
      set { SetValue("Path", value); }
    }

    [Required]
    public DateTime DateTime
    {
      get { return GetValue<DateTime>("DateTime", DateTime.Now); }
      set { SetValue("DateTime", value); }
    }

    [Required]
    public bool IsDeleted
    {
      get { return GetValue<bool>("IsDeleted", false); }
      set { SetValue("IsDeleted", value); }
    }

    public Journal()
      : base()
    {
    }

    public Journal(string path)
      : this()
    {
      Path = path;
    }
  }
}
