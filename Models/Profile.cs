﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using Modman.Utils;

namespace Modman.Models
{
  public class Profile : Model
  {
    [Key]
    public int Id
    {
      get { return GetValue<int>("Id"); }
      set { SetValue("Id", value); }
    }

    [Required]
    public string Name
    {
      get { return GetValue<string>("Name", ""); }
      set { SetValue("Name", value); }
    }

    [Required]
    public string Target
    {
      get { return GetValue<string>("Target", ""); }
      set { SetValue("Target", Path.GetFullPath(value)); }
    }

    [Required]
    public string Source
    {
      get { return GetValue<string>("Source", ""); }
      set
      {
        SetValue("Source", Path.GetFullPath(value))
          .WhenChanged(path =>
            {
              if (_watcher != null)
              {
                _watcher.EnableRaisingEvents = false;
                _watcher.Dispose();
              }

              if (Directory.Exists(path))
              {
                _watcher = new FileSystemWatcher()
                  {
                    Path = Source,
                    NotifyFilter = NotifyFilters.DirectoryName,
                    Filter = "",
                  };
                /*
                _watcher.Created += (s, e) =>
                  {
                    using (var repos = Repository.Begin())
                    {
                      repos.DbContext.Packages.Remove(new Package(this, e.FullPath));
                      repos.DbContext.SaveChanges();
                    }
                  };
                _watcher.Deleted += (s, e) =>
                  {
                    using (var repos = Repository.Begin())
                    {
                      var package = Packages.SingleOrDefault(p => p.Name == e.Name);
                      if (package != null && package.Journals.Count == 0)
                      {
                        repos.DbContext.Packages.Remove(package);
                        repos.DbContext.SaveChanges();
                      }
                    }
                  };
                */
                _watcher.Renamed += (s, e) =>
                  {
                    using (var repos = Repository.Begin())
                    {
                      var package = Packages.SingleOrDefault(p => p.Name == e.OldName);
                      if (package != null)
                      {
                        package.Name = e.Name;
                        repos.DbContext.SaveChanges();
                      }
                    }
                  };
                _watcher.EnableRaisingEvents = true;
              }
            });
      }
    }

    [ForeignKey("Package")]
    public virtual ICollection<Package> Packages
    {
      get { return GetValue<ICollection<Package>>("Packages", new ObservableCollection<Package>()); }
      set { SetValue("Packages", value); }
    }

    FileSystemWatcher _watcher;

    public Profile()
      : base()
    {
    }

    public Profile(string name, string target, string source)
      : this()
    {
      Name = name;
      Target = target;
      Source = source;
    }
  }
}
