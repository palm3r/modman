﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Modman.ViewModels;

namespace Modman.Models
{
  public class Repository : DbContext
  {
    public DbSet<Profile> Profiles { get; set; }
    public DbSet<Package> Packages { get; set; }
    public DbSet<Journal> Journals { get; set; }
    public DbSet<Backup> Backups { get; set; }

    static Repository()
    {
      Database.DefaultConnectionFactory =
        new SqlCeConnectionFactory("System.Data.SqlServerCe.4.0");
#if DEBUG
      Database.SetInitializer(
        new DropCreateDatabaseIfModelChanges<Repository>()
        //new DropCreateDatabaseAlways<Repository>()
        );
#endif
    }

    private Repository()
      : base()
    {
      Configuration.ValidateOnSaveEnabled = false;

      var connStr = string.Format("Data Source={0}.sdf",
      Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().Location));
      Database.Connection.ConnectionString = connStr;
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Profile>()
        .HasMany(profile => profile.Packages)
        .WithRequired(package => package.Profile)
        .HasForeignKey(package => package.ProfileId)
        .WillCascadeOnDelete();

      modelBuilder.Entity<Package>()
        .HasMany(package => package.Journals)
        .WithRequired(journal => journal.Package)
        .HasForeignKey(journal => journal.PackageId)
        .WillCascadeOnDelete();

      base.OnModelCreating(modelBuilder);
    }

    public static RepositoryLocker Begin()
    {
      return new RepositoryLocker();
    }

    public class RepositoryLocker : IDisposable
    {
      public Repository DbContext { get; private set; }
      public ObjectContext ObjectContext { get; private set; }
      static object _lock = new object();

      public RepositoryLocker()
      {
        Monitor.Enter(_lock);
        DbContext = new Repository();
        ObjectContext = ((IObjectContextAdapter)DbContext).ObjectContext;
      }

      public void Dispose()
      {
        DbContext.Dispose();
        Monitor.Exit(_lock);
      }
    }
  }
}
