﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Modman.Utils;

namespace Modman.Models
{
  public enum PackageStatus
  {
    INITIALIZING,
    UNINSTALLED,
    UNINSTALLING,
    INSTALLED,
    INSTALLING,
    INCOMPLETE,
  }

  public class Package : Model
  {
    [Key]
    public int Id
    {
      get { return GetValue<int>("Id"); }
      set { SetValue("Id", value); }
    }

    [ForeignKey("Profile")]
    public int ProfileId
    {
      get { return GetValue<int>("ProfileId"); }
      set { SetValue("ProfileId", value); }
    }

    [ForeignKey("ProfileId")]
    public virtual Profile Profile
    {
      get { return GetValue<Profile>("Profile"); }
      set
      {
        SetValue("Profile", value)
          .WhenChanged(x => RaisePropertyChanged("Path"));
      }
    }

    [Required]
    public string Name
    {
      get { return GetValue<string>("Name", ""); }
      set
      {
        SetValue("Name", value)
          .WhenChanged(x => RaisePropertyChanged("ProgressMaximum"));
      }
    }

    [Required, Column("Status")]
    public string StatusString
    {
      get { return GetValue<string>("StatusString", PackageStatus.INITIALIZING.ToString()); }
      set
      {
        SetValue("StatusString", value)
          .WhenChanged(x => RaisePropertyChanged("Status"));
      }
    }

    [NotMapped]
    public PackageStatus Status
    {
      get
      {
        return _initialized
          ? (PackageStatus)Enum.Parse(typeof(PackageStatus), StatusString)
          : PackageStatus.INITIALIZING;
      }
      set { StatusString = value.ToString(); }
    }

    public ICollection<Journal> Journals
    {
      get { return GetValue<ICollection<Journal>>("Journals", new ObservableCollection<Journal>()); }
      set
      {
        SetValue("Journals", value)
          .WhenChanged(x => ProgressValue = Journals.Count);
      }
    }

    [NotMapped]
    public Visibility Visibility
    {
      get { return GetValue<Visibility>("Visibility", Visibility.Visible); }
      set { SetValue("Visibility", value); }
    }

    [NotMapped]
    public double ProgressValue
    {
      get { return GetValue<double>("ProgressValue"); }
      set { SetValue("ProgressValue", value); }
    }

    [NotMapped]
    public double ProgressMaximum
    {
      /*
      get { return GetValue<double>("ProgressMaximum"); }
      set { SetValue("ProgressMaximum", value); }
      */
      get { return Files.Count(); }
    }

    [NotMapped]
    public TimeSpan ElapsedTime
    {
      get { return GetValue<TimeSpan>("ElapsedTime"); }
      set { SetValue("ElapsedTime", value); }
    }

    [NotMapped]
    public TimeSpan RemainingTime
    {
      get { return GetValue<TimeSpan>("RemainingTime"); }
      set { SetValue("RemainingTime", value); }
    }

    [NotMapped]
    public string Path
    {
      get { return Profile != null ? System.IO.Path.Combine(Profile.Source, Name) : string.Empty; }
    }

    [NotMapped]
    public IEnumerable<FileInfo> Files
    {
      get
      {
        var dir = new DirectoryInfo(Path);
        if (!dir.Exists)
          return new List<FileInfo>();
        return dir.EnumerateFiles("*", SearchOption.AllDirectories);
      }
    }

    [NotMapped]
    public long Size
    {
      get { return Files.Aggregate(0L, (size, file) => size += file.Length); }
    }

    [NotMapped]
    public Command InstallCommand { get; private set; }

    [NotMapped]
    public Command UninstallCommand { get; private set; }

    bool _initialized = false;

    public Package()
      : base()
    {
      InstallCommand = new Command(
        x => Install(),
        x => Status == PackageStatus.UNINSTALLED
        );

      UninstallCommand = new Command(
        x => Uninstall(),
        x => Status == PackageStatus.INSTALLED
        );
    }

    public Package(Profile profile, string path)
      : this()
    {
      Profile = profile;
      Name = System.IO.Path.GetFileName(path);
    }

    public void Initialize()
    {
      Status = PackageStatus.INITIALIZING;
      var files = Files.ToList();

      using (var repos = Repository.Begin())
      {
        repos.DbContext.Packages.Attach(this);
        repos.DbContext.Entry(this).Collection(p => p.Journals).Load();

        ProgressValue = Journals.Count;

        if (ProgressValue == ProgressMaximum)
          Status = PackageStatus.INSTALLED;
        else if (ProgressValue == 0)
          Status = PackageStatus.UNINSTALLED;
        else
          Status = PackageStatus.INCOMPLETE;
      }

      _initialized = true;
      RaisePropertyChanged("Status");
    }

    public Task Install()
    {
      using (var repos = Repository.Begin())
      {
        repos.DbContext.Packages.Attach(this);
        Status = PackageStatus.INSTALLING;
        repos.DbContext.SaveChanges();
      }

      var start = DateTime.Now;
      var task = new Task(() =>
      {
        int i = 0;
        var files = Files.Select(file => file.FullName.Substring(Path.Length + 1)).ToList();
        foreach (var file in files)
        {
          InstallFile(file);
          ElapsedTime = DateTime.Now - start;
          RemainingTime = TimeSpan.FromMilliseconds(
            (ElapsedTime.TotalMilliseconds / ++i) * (files.Count - i));
        }
      });

      // failed
      task.ContinueWith(t =>
      {
        using (var repos = Repository.Begin())
        {
          repos.DbContext.Packages.Attach(this);
          Journals.Select(j => j.Path)
            .ToList()
            .ForEach(path => UninstallFile(path));
          Journals.ToList()
            .ForEach(j => repos.DbContext.Journals.Remove(j));
          Status = PackageStatus.UNINSTALLED;
          repos.DbContext.SaveChanges();
        }
      }, TaskContinuationOptions.OnlyOnFaulted);

      // completed
      task.ContinueWith(t =>
      {
        using (var repos = Repository.Begin())
        {
          repos.DbContext.Packages.Attach(this);
          Status = PackageStatus.INSTALLED;
          ProgressValue = Journals.Count;
          repos.DbContext.SaveChanges();
        }
      }, TaskContinuationOptions.OnlyOnRanToCompletion);

      task.Start();
      return task;
    }

    public Task Uninstall()
    {
      using (var repos = Repository.Begin())
      {
        repos.DbContext.Packages.Attach(this);
        Status = PackageStatus.UNINSTALLING;
        repos.DbContext.SaveChanges();
      }

      var start = DateTime.Now;
      var task = new Task(() =>
      {
        int i = 0;
        var files = Journals.Select(j => j.Path).ToList();
        foreach (var file in files)
        {
          UninstallFile(file);
          ElapsedTime = DateTime.Now - start;
          RemainingTime = TimeSpan.FromMilliseconds(
            (ElapsedTime.TotalMilliseconds / ++i) * (files.Count - i));
        }
      });

      // failed
      task.ContinueWith(t =>
      {
        using (var repos = Repository.Begin())
        {
          repos.DbContext.Packages.Attach(this);
          Journals.Where(j => j.IsDeleted)
            .Select(j => j.Path)
            .ToList()
            .ForEach(path => InstallFile(path));
          Status = PackageStatus.INSTALLED;
          repos.DbContext.SaveChanges();
        }
      }, TaskContinuationOptions.OnlyOnFaulted);

      // completed
      task.ContinueWith(t =>
      {
        using (var repos = Repository.Begin())
        {
          repos.DbContext.Packages.Attach(this);
          Journals.ToList()
            .ForEach(j => repos.DbContext.Journals.Remove(j));
          ProgressValue = 0;
          Status = PackageStatus.UNINSTALLED;
          repos.DbContext.SaveChanges();
        }
      }, TaskContinuationOptions.OnlyOnRanToCompletion);

      task.Start();
      return task;
    }

    private void InstallFile(string path)
    {
      var source = System.IO.Path.Combine(Profile.Source, Name, path);
      var target = System.IO.Path.Combine(Profile.Target, path);
      var dir = System.IO.Path.GetDirectoryName(target);

      if (!Directory.Exists(dir))
        Directory.CreateDirectory(dir);

      using (var repos = Repository.Begin())
      {
        repos.DbContext.Packages.Attach(this);

        var journal = Journals.SingleOrDefault(j => j.Path == path && !j.IsDeleted);
        if (journal == null)
        {
          Journals.Add(new Journal(path));
          repos.DbContext.SaveChanges();
        }

        var journalOfOtherPackage = repos.DbContext.Journals
          .Include(j => j.Package)
          .OrderBy(j => j.DateTime)
          .ToList()
          .LastOrDefault(j => j.Package.Id != Id && j.Path == path && !j.IsDeleted);

        if (journalOfOtherPackage == null && File.Exists(target))
        {
          var backup = Backup.Save(target, Profile.Target);
          repos.DbContext.Backups.Add(backup);
          repos.DbContext.SaveChanges();
        }
      }

      if (Hash.FromFile(source) != Hash.FromFile(target))
        FileManager.Install(source, target, true);

      ProgressValue++;
    }

    private void UninstallFile(string path)
    {
      var source = System.IO.Path.Combine(Profile.Source, Name, path);
      var target = System.IO.Path.Combine(Profile.Target, path);
      var dir = System.IO.Path.GetDirectoryName(target);

      FileManager.Delete(target);

      using (var repos = Repository.Begin())
      {
        repos.DbContext.Packages.Attach(this);

        var journal = Journals.SingleOrDefault(j => j.Path == path && !j.IsDeleted);
        if (journal != null)
        {
          journal.IsDeleted = true;
          repos.DbContext.SaveChanges();
        }

        journal = repos.DbContext.Journals
          .Where(j => j.Package.Id != Id && j.Path == path)
          .ToList()
          .OrderBy(j => j.DateTime)
          .LastOrDefault();
        if (journal != null)
        {
          var recover = System.IO.Path.Combine(Profile.Source, journal.Package.Name, journal.Path);
          FileManager.Install(recover, target, true);
        }
        else
        {
          var backup = repos.DbContext.Backups
            .SingleOrDefault(b => b.Path == path);
          if (backup != null)
          {
            backup.Restore(target);
            repos.DbContext.Backups.Remove(backup);
            repos.DbContext.SaveChanges();
          }
        }
      }

      var di = new DirectoryInfo(dir);
      while (di.FullName != Profile.Target &&
        di.Exists && di.EnumerateFiles("*", SearchOption.AllDirectories).Count() == 0)
      {
        di.Delete(true);
        di = di.Parent;
      }

      ProgressValue--;
    }
  }
}
