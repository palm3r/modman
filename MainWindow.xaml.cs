﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Modman.ViewModels;
using Modman.Models;

namespace Modman
{
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
    {
      var vm = DataContext as MainWindowViewModel;
      if (vm.Profiles.SelectMany(profile => profile.Packages)
        .SingleOrDefault(package => package.Status == PackageStatus.INSTALLING
          || package.Status == PackageStatus.UNINSTALLING) != null)
      {
        e.Cancel = true;
      }

      base.OnClosing(e);
    }
  }
}
