﻿Modman v1.0.1
Copyright (C) 2011 Palm3r All rights reserved.
https://bitbucket.org/palm3r/modman

------------------------------
What's this?
------------------------------

This is a MOD management software like JSGME.

Modman can manage MOD of various games by the same way as JSGME.
Also Modman install MOD files using Symbolic link function implemented from Windows Vista.
Therefore, you can hold down consumption of a hard disk.

About JSGME, see below URL.
(http://www.users.on.net/~jscones/software/products-jsgme.html)

------------------------------
Requirements
------------------------------

Microsoft .NET Framework 4 Client Profile
	http://www.microsoft.com/download/en/details.aspx?id=24872

Windows Vista or later (Recommended)
	This software will work on Windows XP or older version.
	However, Symbolic link function does not work.
	It means you need twice larger space for installing.

------------------------------
How to use
------------------------------

1. Prepare your MODS directory
	Like JSGME, you need to prepare MODS directory.
	Directory layout should be following:

		MODS (You can choose directory name freely)
			+ MOD1 (Directory name is displayed as MOD name on UI)
				+ mod_subdir
					+ mod_file3
				+ mod_file1
				+ mod_file2
			+ MOD2
				....

2. Create new profile.
	Name: Profile name needs to be unique.
	Target: A path to the folder to which MODs are installed.
	Source: A path to the folder in which MOD before being installed is stored.

3. Install or Uninstall MODs

4. If you want to uninstall Modman itself, just delete all files from directory which Modman installed.
	However, when some MODs are already installed, before uninstalling Modman,
	I recommend you to uninstall them previously strongly. 

------------------------------
Changelog
------------------------------

v1.0.1: 2011-10-10
- Fixed: empty directory was shown as installed package
- Fixed: Filter does not hide packages completely

v1.0: 2011-10-10
- First release
